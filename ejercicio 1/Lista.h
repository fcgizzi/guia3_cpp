// libreria
#include <iostream>
using namespace std;

// nodo
#include "main.h"


#ifndef LISTA_H
#define LISTA_H

// constructores de la clase lista
class Lista{
    // privaods
    private:
        Nodo *inicio = 0;
        Nodo *ultimo = 0;

    //publicos
    public:
        Lista();
        void imprimir();
        void ordenar_lista(Nodo *temporal);
        void creacion_nodo(int dato);
};
#endif

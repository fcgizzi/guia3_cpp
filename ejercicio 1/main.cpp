/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */

// librería
#include <iostream>
using namespace std;

// clase
#include "Lista.h"

// función principal
int main(){
    // lista creada
    Lista *lista = new Lista();
    // comienza con uno para que entre en el ciclo while
    string opcion = "s";
    int dato;
    cout << "---Lista enlazada---" << endl;
	
	// mientras la respuesta ingreasa sea s
	// se seguiran agregando datos
    while(opcion == "s"){
        cout << ">> Ingresar dato:  ";
        cin >> dato;
		cout << "\t" << endl;
        // se agregan a la lista
        // llamando a la clase
        lista-> creacion_nodo(dato);
        // llama a función imprimir de la clase
        lista-> imprimir();
		cout << "\n------------------------" << endl;
        cout << "| s >> Sí              |" << endl;
        cout << "| n >> No, salir       |" << endl;
        cout << "------------------------" << endl;
        cout << ">> ";
        cin >> opcion;
    }
    if(opcion != "s"){
		cout << "\n\t --ADIOS--" << endl;
	}
    return 0;
}

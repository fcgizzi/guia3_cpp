// libreria
#include <iostream>
using namespace std;

// clase
#include "Lista.h"

// constructor de clase
Lista::Lista(){

}

// funcion imprime lista
void Lista::imprimir(){
    // con la variable temporal se recorre la lista
    Nodo *temp = this-> inicio;
    cout << "---Lista---" << endl;

    // si es distinto a NULL la recorre
    while (temp != NULL){
        cout << temp-> dato << " | ";
        temp = temp-> nodo_siguiente;
    }
}

// función ordena lista de manera ascendente
void Lista::ordenar_lista(Nodo *temp){
    // variable auxiliar
    int aux;
    // mientras temp sea distinto a NULL
    // recorre
    while(temp != NULL){
		// se usa una segunda variable temporal
        Nodo *temp2 = temp-> nodo_siguiente;
        while(temp2 != NULL){
			if(temp-> dato > temp2-> dato){
				aux = temp2-> dato;
				temp2-> dato = temp-> dato;
				temp-> dato = aux;
			}
			temp2 = temp2 -> nodo_siguiente;
		}
		temp = temp-> nodo_siguiente;
	}
}

// función creacion de nodo
void Lista::creacion_nodo(int dato){
    Nodo *temp = new Nodo;
	
	// variable temporal para espacio de memoria
    temp -> dato = dato;
    temp -> nodo_siguiente = NULL;
	
	// primer nodo se deja como último
	// sino se deja el nuevo como el último
    if(this->inicio == NULL){
		this-> inicio = temp;
		this->ultimo = this->inicio;
    }
    else{
        this-> ultimo-> nodo_siguiente = temp;
        this-> ultimo = temp;
    }
    // llama a la funcion que orde la lista
    ordenar_lista(this-> inicio);
}

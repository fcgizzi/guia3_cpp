# Lista ordenada

Se pide ingresar datos en una lista, para que estos posteriormente sean ordenados de manera ascendente

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal

- Si no se tiene make, se usa `g++ main.cpp Lista.cpp -o main` para compilar

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal

# Programa

Al iniciar, el programa solicita ingresar datos constantementea hasta que el usuario lo detenga.
Lurgo decada ingreso, se imprime la que se lleva de la lista, de manera ordenada

# Requisitos

- Sistema operativo Linux

- Make instalado

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi
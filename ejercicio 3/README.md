# Completado de Lista

Se solicita un programa al que se ingresen datos numéricos en una lista, para que este, posteriormente entregue una lista completando con los datos faltantes y ordenados de manera ascendente. 

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal
- Si no se tiene make, se usa `g++ main.cpp Lista.cpp -o main para compilar`

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal

# Programa

Inicia el progama con un menú de opciones las cuales son: agregar datos, ver lista y salir.
Con la primera opción este pedirá datos volviendo al menú luego del ingreso de este.
Con la segunda opción, entregará la lista con los datos faltantes y de manera ordenada.
La última opción hace que salga del programa.

# Requisitos

- Sistema operativo Linux

- Make instalado

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi
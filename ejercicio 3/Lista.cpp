// librería
#include <iostream>
using namespace std;

//clases
#include "main.h"
#include "Lista.h"

// constructor de lista
Lista::Lista(){}

// función que imprime la lista
void Lista::imprime_lista() {
	// se usa variable demporal para recorrer
	Nodo *temp = this-> inicio;
	// cuando es diferente a NULL recorre ciclo
	while (temp != NULL) {
		cout << temp-> dato << "|";
		temp = temp-> siguiente;
	}
	cout << "\n";
}

// función que añade dato a la lista
void Lista::anadir_dato(int dato){
	Nodo *temp;
    // creación variable temporal nodo
    // a la que se asigna un valor
    temp = new Nodo;
    temp-> dato = dato;
    temp-> siguiente = NULL;

    //primer nodo queda primero y como último
    // sino, se usa el último y se deja como último
    if (this-> inicio == NULL) {
        this-> inicio = temp;
        this-> ultimo = this-> inicio;
    }
    else{
		this-> ultimo-> siguiente = temp;
		this-> ultimo = temp;
    }
}

//función que ordena la lista
void Lista::ordena_lista(){
	//variable temporal
	// y varible de orden
	Nodo *temp= this-> inicio;
	int aux;
	// temp debe ser distinta a NULL para que recorra el ciclo
	while(temp != NULL){
		Nodo *temp2 = temp-> siguiente;
		// se guarda nueva variable temporal como siguiente
		// si el siguiente no es el último entra en el ciclo
		while(temp2 != NULL){
			// se busca el dato enomr
			if(temp-> dato > temp2-> dato){
				// se usa la variable auxiliar para ordenar 
				aux = temp2-> dato;
				temp2-> dato = temp-> dato;
				temp-> dato = aux;
			}
			temp2 = temp2-> siguiente;
		}
		temp = temp-> siguiente;
	}
}

// función que agrega asignando nuevos nodos
void Lista::agrega(Nodo *temp){
	// se crea nuevo nodo
	// se lama a la variable temporal
	Nodo *nuevo = new Nodo;
	nuevo-> dato = temp-> dato + 1;
	nuevo-> siguiente = temp-> siguiente;
	temp-> siguiente = nuevo;
}

// función que busca los datos que faltan
void Lista::busca_vacio(){
	// variable nodo temporal para recorrer
	Nodo *temp = this-> inicio;
	busca_vacio();
	// mientras dea distinto a NULL recorre
	while(temp!=NULL){
		Nodo *temp2 = temp-> siguiente;
		if(temp2 != NULL){
			// se restan dos datos
			// para conocer la cantidad faltante
			temp-> cant_falt = temp2-> dato - temp-> dato;
		}
		temp = temp -> siguiente;
	}
}

// función que llena la lista con los datos faltante
void Lista::complementa_lista(){
	// variable temporal para recorrer
	Nodo *temp = this-> inicio;
	// llama a la función para saber cuantos faltan por llenar
	busca_vacio();
	// se recorre la lista
	for(int i=1; temp != NULL; i++){
		// la distancia debe ser mayor a 1 para agregar
		if(temp-> cant_falt > 1){
			agrega(temp);
		}
		temp = temp-> siguiente;
		// se vuelve a llamar la función
		// para que se evalúe completamente
		busca_vacio();
	}
}

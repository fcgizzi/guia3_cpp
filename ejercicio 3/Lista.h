// libreria
#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H
// clase lista
// con sus estructuras
class Lista {
    private:
        Nodo *inicio = NULL;
        Nodo *ultimo = NULL;
    public:
        Lista();
        // funciones
        void ordena_lista();
        void anadir_dato(int dato);
        void imprime_lista();
        void agrega(Nodo *tmp);
        void busca_vacio();
        void complementa_lista();
};
#endif

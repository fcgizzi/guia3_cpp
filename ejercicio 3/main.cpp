/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */
// librería
#include <iostream>
using namespace std;

// estructura nodo
#include "main.h"

// clase
#include "Lista.h"

// clase programa
class Main {
	private:
		Lista *lista = NULL;
	public:
		Main() {
		this-> lista = new Lista();
    }
    // se entrega una lista
    Lista *get_lista() {
		return this-> lista;
    }
};

// función que pide dato de ingreso
void ingreso_dato(Lista *lista){
  // variable de número a agregar
  int agregar;
  cout << "Dato a agregar: ";
  cin >> agregar;

  // se agrega el número
  lista-> anadir_dato(agregar);
}

// función menu
void menu(Lista *lista){
	// variable por si quiere o no agregar algo
	int op;
	cout << "----------------------" << endl;
	cout << "| --Ingrese opción-- |" << endl;
	cout << "| 1 >>  Agregar      |" << endl;
	cout << "| 2 >> Ver lista     |" << endl;
	cout << "| otro >>  Salir     |" << endl;
	cout << "----------------------" << endl;
	cout << ">> ";
	cin >> op;

	// cuando se ingresa esta opción se llamaa ingresar dato
	// luego se vuelve al menú
	if(op == 1){
		ingreso_dato(lista);
		menu(lista);
	}
	// 
	else if(op == 2 ){
		// funciones de clase
		// se imprime la lista ordenada 
		// y la lista complementada
		lista-> ordena_lista();
		lista-> imprime_lista();
		lista-> complementa_lista();
		lista-> imprime_lista();
		menu(lista);
	}
	else{
		cout << "\t--Adiós--" << endl;
	}
}

// función principal
int main(void){
	// se crea el programa main
	// y se extrae la lista
	Main m = Main();
	Lista *lista = m.get_lista();

	// se llama al menú y se envía lista
	menu(lista);
}

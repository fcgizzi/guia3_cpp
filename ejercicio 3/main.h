// estructura del nodo
typedef struct _Nodo {
    int dato;
    // vacío existentre entre datos
    int cant_falt;
    struct _Nodo *siguiente;
} Nodo;

# Enlazado de Listas y Orden

Se solicita un programa que solicite el ingreso a dos listas diferentes, las cuales, posteriormente se enlazarán formando una lista nueva y ordenada

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal
- Si no se tiene make, se usa `g++ main.cpp Lista.cpp -o main para compilar`

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal

# Programa

Al iniciar el progama este lanza un menú de opciones las cuales son: agregar datos, ver lista y salir.
La primera opción lleva a otro menú, el cual tendra la opción de ingreso a la lista uno y de la lista dos.
Después de cada ingreso se retorna al menú para nuevamente escoger opciones.
La opción de visualizar lista permite exactamente eso, muestra las dos listas ingresadadas, ordenadas y la tercera lista mezclada y ordenada.
La última opción permite abandonar el programa.

# Requisitos

- Sistema operativo Linux

- Make instalado

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi
// libreria
#include <iostream>
using namespace std;
// nodo
#include "main.h"

// constructores
#ifndef LISTA_H
#define LISTA_H

// clase lista
class Lista{
    private:
        Nodo *inicio = 0;
        Nodo *ultimo = 0;
    public:
        Lista();
        void crea_nodo(int dato);
        void ordena_lista(Nodo *temp);
        void imprime_lista();
        Nodo* get_inicio();
};
#endif

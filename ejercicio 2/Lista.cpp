//libreria
#include <iostream>
using namespace std;
//clase
#include "Lista.h"

// constructor de lista
Lista::Lista(){
}

// función que imprime lista
void Lista::imprime_lista(){
    // variable temporal para recorrer mientra sea distinto a NULL
    Nodo *temp = this-> inicio;
    while (temp != NULL){
        cout << temp-> dato << " | ";
        temp = temp-> nodo_siguiente;
    }
    cout << "\n" << endl;
}

// función creadora de nodo
void Lista::crea_nodo(int dato){
	// variable t4emporal para espacio
    Nodo *temp = new Nodo;
    temp -> dato = dato;
    temp -> nodo_siguiente = NULL;

    // primer nodo se deja como primero y último
    // sino, el actual ultimo se deja como ultimo de lista
    if(this-> inicio == NULL){
		this-> inicio = temp;
		this-> ultimo = this-> inicio;
    }
    else{
        this-> ultimo-> nodo_siguiente = temp;
        this-> ultimo = temp;
    }
    // llama a función de ordenación de lista
    ordena_lista(this-> inicio);
}

//función ordena lista de menor a mayor
void Lista::ordena_lista(Nodo *temp){
    // variable auxiliar
    int aux;
    // recorre mientras sea distinto a NULL
    while(temp != NULL){
        Nodo *temp2 = temp-> nodo_siguiente;
        // abre un ciclo para temporal 2
        // y se ordena de menor a mayor usando variable auxiliar
        while(temp2 != NULL){
			if(temp-> dato > temp2-> dato){
				aux = temp2-> dato;
				temp2-> dato = temp-> dato;
				temp-> dato = aux;
			}
			temp2 = temp2-> nodo_siguiente;
		}
		temp = temp-> nodo_siguiente;
    }
}

// se retorna el incio de la lista
Nodo* Lista::get_inicio(){
    return this-> inicio;
}

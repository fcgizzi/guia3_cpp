/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */
// libreria
#include <iostream>
using namespace std;
// clase
#include "Lista.h"

// función que combina ambas listas
void lista_combinada(Lista *lista, Lista *lista3){
    int dato;
    // entrega el inicio de la lista
    Nodo *temp = lista-> get_inicio();

    // mientras sea distinto a NULL, recorre
    while(temp != NULL){
        // se van agregando a la nueva lista
        dato = temp-> dato;
        lista3-> crea_nodo(dato);

        // nodo siguiente
        temp = temp-> nodo_siguiente;
    }
}

// función para ingresar datos
void ingresar_datos(Lista *lista){
    int dato;

    cout << ">> Dato que ingresará en la lista: ";
    cin >> dato;
    //Se crea y se añade los numeros a la lista
    lista-> crea_nodo(dato);

}

// función menú de ingreso según lista
void menu_dos(Lista *lista1, Lista *lista2, Lista *lista3){
	int op;
	cout << ">> Escoger lista a la cual desea agregar datos" << endl;
	cout << "\t--------------------" << endl;
	cout << "\t| 1 >> Lista 1     |" << endl;
    cout << "\t| 2 >> Lista 2     |" << endl;
    cout << "\t--------------------" << endl;
    cout << "\t>> ";
    cin >> op;
    
    // ambas opciones llaman a ingresar datos
    // con su respectiva lista
    // luego esta es combinada llamando a la funcion de combinacion
    // ahí se añaden los datos a la lista 3
    if(op == 1){
		// lista 1
        ingresar_datos(lista1);
        lista_combinada(lista1, lista3);
	}
	else {
		// lista 2
        ingresar_datos(lista2);
        lista_combinada(lista2, lista3);
	}   
}

// función primer menú
void menu(Lista *lista1, Lista *lista2, Lista *lista3){
    int op;

    cout << "\n\t\t--- Menú---" << endl;
    cout << "\t-----------------------------" << endl;
    cout << "\t| 1 - Agregar dato en lista |" << endl;
    cout << "\t| 2 - Ver todas las listas  |" << endl;
    cout << "\t| 3 - Salir                 |" << endl;
    cout << "\t-----------------------------" << endl;
    cout << "\t>> ";
    cin >> op;
	
	// opción que llama al menú de listas
    if(op == 1){
		menu_dos(lista1, lista2, lista3);
		//se vuelve al menú
		menu(lista1, lista2, lista3);
    }
    // opción imprime listas
    // llamando de la clase
    if(op == 2){
		cout << "\t --Listas ordenadas--";
        cout << "Lista 1: " << endl;
        lista1-> imprime_lista();
        cout << "Lista 2: " << endl;
        lista2-> imprime_lista();
        cout << "Lista combinada: " << endl;
        lista3-> imprime_lista();
        // se vuelve al menú
        menu(lista1, lista2, lista3);
    }
    else{
        cout << "\t --Adiós--" << endl;
    }
}

// función principal
int main(){
    // listas creadas
    // lista 3 es la combinada
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();
	// se va a un menú de opciones
    menu(lista1, lista2, lista3);
}
